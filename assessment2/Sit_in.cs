﻿//Calum McCall
//This class contains the Sit_in class, its paramaters and verification. This class inherits from the Order class
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    //This class inherits from the Order class
    class Sit_in : Order
    {
        private int table;  //The table for the order
        private List<Menu_item> sit_in_orders = new List<Menu_item>();  //List of all sit in orders

        public int Table
        //Initialisng and allowing access to table
        {
            get
            {
                return table;
            }
            set
            {
                //Ensures value is between 1 and 10
                if ((value < 1) || (value > 10))
                {
                    throw new ArgumentException("Invalid table entered");
                }
                //Sets to value if valid number
                table = value;
            }
        }

        public List<Menu_item> getItem()
        {
            return sit_in_orders;
        }

        public void addItems(List<Menu_item> i)
        {
            sit_in_orders.AddRange(i);
        }
    }
}
