﻿//Calum McCall
//This class contains the Order class, its paramaters and verification.
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class Order
    {
        //Creates lists of all the information
        private List<Employee> employee_list = new List<Employee>();
        public List<Menu_item> menu_item_list = new List<Menu_item>();
        private List<Sit_in> sit_in_list = new List<Sit_in>();
        private List<Delivery> delivery_list = new List<Delivery>();
        private List<Menu_item> order = new List<Menu_item>();

        public Order()
        {

        }

        //Return a list of the items on the menu
        public List<Menu_item> menu()
        {
            return menu_item_list;
        }

        //Displays the menu items and all of their properties
        public string display_menu(int i)
        {
            return menu_item_list[i].Description + menu_item_list[i].Price + "\t" + Veg(menu_item_list[i].Vegetarian);
        }

        //Used to display if the menu item is vegetarian or not
        public string Veg(bool vegetarian)
        {
            if (vegetarian)
            {
                return "Yes";
            }

            return "No";
        }

        //Add new item to the list of menu items
        public void add_menu_item(Menu_item add_menu_item)
        {
            menu_item_list.Add(add_menu_item);
        }

        //Remove an item from the list of menu items
        public void remove_menu_item(int i)
        {
            menu_item_list.RemoveAt(i);
        }

        //Add a new sit in order
        public void new_sit_in(Sit_in new_sit_in)
        {
            sit_in_list.Add(new_sit_in);
        }

        //Displays all sit in orders
        public List<Sit_in> display_sit_in()
        {
            return sit_in_list;
        }

        //Add a new delivery order
        public void new_delivery(Delivery new_delivery)
        {
            delivery_list.Add(new_delivery);
        }

        //Displays all delivery orders
        public List<Delivery> display_delivery()
        {
            return delivery_list;
        }

        //List of the servers
        public List<Server> server_list()
        {
            List<Server> populate_server = new List<Server>();

            foreach (var server in employee_list)
            {
                if (server.GetType() == typeof(Server))
                {
                    populate_server.Add((Server)server);
                }
            }

            return populate_server;
        }

        //List of the drivers
        public List<Driver> driver_list()
        {
            List<Driver> populate_driver = new List<Driver>();

            foreach (var driver in employee_list)
            {
                if (driver.GetType() == typeof(Driver))
                {
                    populate_driver.Add((Driver)driver);
                }
            }

            return populate_driver;
        }

        //This method will add a new employee
        public void new_employee(Employee new_employee)
        {
            employee_list.Add(new_employee);
        }

        //This method will remove a server
        public void remove_server(int i)
        {
            employee_list.RemoveAt(i);
        }

        //This method will remove a driver
        public void remove_driver(int i)
        {
            employee_list.RemoveAt(i);
        }

        //Counts the number of servers, used for displaying them
        public int count_server()
        {
            int count = 0;
            foreach (Employee i in employee_list)
            {
                if (i is Server)
                {
                    count++;
                }
            }

            return count;
        }

        //Displays the server and their information
        public string display_server(int i)
        {
            string name = "";
            int count = 0;
            foreach (Employee s in employee_list)
            {
                if (s is Server)
                {
                    if (count == i)
                    {
                        name = s.First_name + "\t" + s.Staff_id;
                    }

                    count++;
                }
            }

            return name;
        }

        //Counts the number of drivers, used for displaying them
        public int count_driver()
        {
            int count = 0;
            foreach (Employee i in employee_list)
            {
                if (i is Driver)
                {
                    count++;
                }
            }

            return count;
        }

        //Displays the server and their information
        public string display_driver(int i)
        {
            string name = "";
            int count = 0;

            foreach (Employee s in employee_list)
            {
                if (s is Driver)
                {
                    if (count == i)
                    {
                        name = s.First_name + "\t" + s.Staff_id;
                    }

                    count++;
                }
            }

            return name;
        }
    }
}