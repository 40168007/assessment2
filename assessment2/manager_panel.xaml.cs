﻿//Calum McCall
//This class contains the MainWindow class, its paramaters and verification.
//Last modified 10/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for manager_panel.xaml
    /// </summary>
    public partial class manager_panel : Window
    {
        //Creating a copy of the order class for this window
        Order manager_order = new Order();
        internal manager_panel(Order order)
        {
            InitializeComponent();

            //Put the contents of order into the order for this window
            manager_order = order;

            item_list.ItemsSource = manager_order.menu();
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void alter_items_Click(object sender, RoutedEventArgs e)
        {
            //Setting the relevant UI displayed
            add_menu_item.Visibility = Visibility.Visible;
            amend_menu_item.Visibility = Visibility.Visible;
            remove_menu_item.Visibility = Visibility.Visible;
            add_server.Visibility = Visibility.Hidden;
            amend_server.Visibility = Visibility.Hidden;
            remove_server.Visibility = Visibility.Hidden;
            add_driver.Visibility = Visibility.Hidden;
            amend_driver.Visibility = Visibility.Hidden;
            remove_driver.Visibility = Visibility.Hidden;
            name_label.Visibility = Visibility.Hidden;
            name_input.Visibility = Visibility.Hidden;
            id_label.Visibility = Visibility.Hidden;
            id_input.Visibility = Visibility.Hidden;
            confirm_server.Visibility = Visibility.Hidden;
            registration_input.Visibility = Visibility.Hidden;
            registration_label.Visibility = Visibility.Hidden;
            confirm_driver.Visibility = Visibility.Hidden;
            driver_list_box.Visibility = Visibility.Hidden;
            remove_driver1.Visibility = Visibility.Hidden;
            server_list_box.Visibility = Visibility.Hidden;
            remove_server1.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Hidden;

        }

        private void alter_servers_Click(object sender, RoutedEventArgs e)
        {
            //Setting the relevant UI displayed
            add_menu_item.Visibility = Visibility.Hidden;
            amend_menu_item.Visibility = Visibility.Hidden;
            remove_menu_item.Visibility = Visibility.Hidden;
            add_server.Visibility = Visibility.Visible;
            amend_server.Visibility = Visibility.Visible;
            remove_server.Visibility = Visibility.Visible;
            add_driver.Visibility = Visibility.Hidden;
            amend_driver.Visibility = Visibility.Hidden;
            remove_driver.Visibility = Visibility.Hidden;
            description_label.Visibility = Visibility.Hidden;
            description_input.Visibility = Visibility.Hidden;
            price_label.Visibility = Visibility.Hidden;
            price_input.Visibility = Visibility.Hidden;
            vegetarian_label.Visibility = Visibility.Hidden;
            yes_choice.Visibility = Visibility.Hidden;
            no_choice.Visibility = Visibility.Hidden;
            confirm_add_item.Visibility = Visibility.Hidden;
            confirm_amend_item.Visibility = Visibility.Hidden;
            item_list.Visibility = Visibility.Hidden;
            remove_item.Visibility = Visibility.Hidden;
            registration_input.Visibility = Visibility.Hidden;
            registration_label.Visibility = Visibility.Hidden;
            confirm_driver.Visibility = Visibility.Hidden;
            driver_list_box.Visibility = Visibility.Hidden;
            remove_driver1.Visibility = Visibility.Hidden;
            server_list_box.Visibility = Visibility.Hidden;
            remove_server1.Visibility = Visibility.Hidden;
            name_input.Visibility = Visibility.Hidden;
            name_label.Visibility = Visibility.Hidden;
            id_input.Visibility = Visibility.Hidden;
            id_label.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Hidden;
        }

        private void alter_drivers_Click(object sender, RoutedEventArgs e)
        {
            //Setting the relevant UI displayed
            add_menu_item.Visibility = Visibility.Hidden;
            amend_menu_item.Visibility = Visibility.Hidden;
            remove_menu_item.Visibility = Visibility.Hidden;
            add_server.Visibility = Visibility.Hidden;
            amend_server.Visibility = Visibility.Hidden;
            remove_server.Visibility = Visibility.Hidden;
            add_driver.Visibility = Visibility.Visible;
            amend_driver.Visibility = Visibility.Visible;
            remove_driver.Visibility = Visibility.Visible;
            description_label.Visibility = Visibility.Hidden;
            description_input.Visibility = Visibility.Hidden;
            price_label.Visibility = Visibility.Hidden;
            price_input.Visibility = Visibility.Hidden;
            vegetarian_label.Visibility = Visibility.Hidden;
            yes_choice.Visibility = Visibility.Hidden;
            no_choice.Visibility = Visibility.Hidden;
            confirm_add_item.Visibility = Visibility.Hidden;
            confirm_amend_item.Visibility = Visibility.Hidden;
            item_list.Visibility = Visibility.Hidden;
            remove_item.Visibility = Visibility.Hidden;
            name_label.Visibility = Visibility.Hidden;
            name_input.Visibility = Visibility.Hidden;
            id_label.Visibility = Visibility.Hidden;
            id_input.Visibility = Visibility.Hidden;
            confirm_server.Visibility = Visibility.Hidden;
            registration_input.Visibility = Visibility.Hidden;
            registration_label.Visibility = Visibility.Hidden;
            confirm_driver.Visibility = Visibility.Hidden;
            driver_list_box.Visibility = Visibility.Hidden;
            remove_driver1.Visibility = Visibility.Hidden;
            server_list_box.Visibility = Visibility.Hidden;
            remove_server1.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Hidden;
        }

        private void add_menu_item_Click(object sender, RoutedEventArgs e)
        {
            description_label.Visibility = Visibility.Visible;
            description_input.Visibility = Visibility.Visible;
            price_label.Visibility = Visibility.Visible;
            price_input.Visibility = Visibility.Visible;
            vegetarian_label.Visibility = Visibility.Visible;
            yes_choice.Visibility = Visibility.Visible;
            no_choice.Visibility = Visibility.Visible;
            confirm_add_item.Visibility = Visibility.Visible;
            confirm_amend_item.Visibility = Visibility.Hidden;
            item_list.Visibility = Visibility.Hidden;
            remove_item.Visibility = Visibility.Hidden;
        }

        private void confirm_add_item_click(object sender, RoutedEventArgs e)
        {
            description_label.Visibility = Visibility.Hidden;
            description_input.Visibility = Visibility.Hidden;
            price_label.Visibility = Visibility.Hidden;
            price_input.Visibility = Visibility.Hidden;
            vegetarian_label.Visibility = Visibility.Hidden;
            yes_choice.Visibility = Visibility.Hidden;
            no_choice.Visibility = Visibility.Hidden;
            confirm_add_item.Visibility = Visibility.Hidden;

            //Create a new item for the menu
            Menu_item new_item = new Menu_item();
            //Set the input content to the right variables
            new_item.Description = description_input.Text + " \t $";
            new_item.Price = double.Parse(price_input.Text);
            if (yes_choice.IsChecked == true)
            {
                new_item.Vegetarian = true;
                yes_choice.IsChecked = false;
            }
            else
            {
                new_item.Vegetarian = false;
                no_choice.IsChecked = false;
            }
            manager_order.add_menu_item(new_item);
            description_input.Clear();
            price_input.Clear();
        }

        private void amend_menu_item_Click(object sender, RoutedEventArgs e)
        {
            description_label.Visibility = Visibility.Visible;
            description_input.Visibility = Visibility.Visible;
            price_label.Visibility = Visibility.Visible;
            price_input.Visibility = Visibility.Visible;
            vegetarian_label.Visibility = Visibility.Visible;
            yes_choice.Visibility = Visibility.Visible;
            no_choice.Visibility = Visibility.Visible;
            confirm_add_item.Visibility = Visibility.Hidden;
            confirm_amend_item.Visibility = Visibility.Visible;
            item_list.Visibility = Visibility.Visible;
            remove_item.Visibility = Visibility.Hidden;
        }

        private void confirm_amend_item_click(object sender, RoutedEventArgs e)
        {
            description_label.Visibility = Visibility.Hidden;
            description_input.Visibility = Visibility.Hidden;
            price_label.Visibility = Visibility.Hidden;
            price_input.Visibility = Visibility.Hidden;
            vegetarian_label.Visibility = Visibility.Hidden;
            yes_choice.Visibility = Visibility.Hidden;
            no_choice.Visibility = Visibility.Hidden;
            confirm_amend_item.Visibility = Visibility.Hidden;

            //Does not work correctly
            String amend_item = item_list.SelectedItem.ToString();
            //string[] words = amend_item.Split("\t");
            //description_input.Text = words[0];
            //price_input.Text = words[1];
        }

        private void remove_menu_item_Click(object sender, RoutedEventArgs e)
        {
            description_label.Visibility = Visibility.Hidden;
            description_input.Visibility = Visibility.Hidden;
            price_label.Visibility = Visibility.Hidden;
            price_input.Visibility = Visibility.Hidden;
            vegetarian_label.Visibility = Visibility.Hidden;
            yes_choice.Visibility = Visibility.Hidden;
            no_choice.Visibility = Visibility.Hidden;
            confirm_amend_item.Visibility = Visibility.Hidden;
            item_list.Visibility = Visibility.Visible;
            remove_item.Visibility = Visibility.Visible;
        }

        private void remove_item_Click(object sender, RoutedEventArgs e)
        {
            //Remove the selected item from the list
            manager_order.remove_menu_item(item_list.SelectedIndex);
            item_list.ItemsSource = null;
            item_list.ItemsSource = manager_order.menu();
        }

        private void add_server_Click_1(object sender, RoutedEventArgs e)
        {
            name_label.Visibility = Visibility.Visible;
            name_input.Visibility = Visibility.Visible;
            id_label.Visibility = Visibility.Visible;
            id_input.Visibility = Visibility.Visible;
            confirm_server.Visibility = Visibility.Visible;
            remove_server1.Visibility = Visibility.Hidden;
            server_list_box.Visibility = Visibility.Hidden;
        }

        private void confirm_server_Click(object sender, RoutedEventArgs e)
        {
            //Add a new server
            Server new_server = new Server();
            //Set the relevant input to the right variables
            new_server.First_name = name_input.Text;
            new_server.Staff_id = Int32.Parse(id_input.Text);

            server_list_box.ItemsSource = null;
            manager_order.new_employee(new_server);
            server_list_box.ItemsSource = manager_order.server_list();

            name_input.Clear();
            id_input.Clear();
        }

        private void amend_server_Click(object sender, RoutedEventArgs e)
        {
            name_input.Visibility = Visibility.Visible;
            name_label.Visibility = Visibility.Visible;
            id_input.Visibility = Visibility.Visible;
            id_label.Visibility = Visibility.Visible;
            confirm_server.Visibility = Visibility.Visible;
            server_list_box.Visibility = Visibility.Visible;
            remove_server1.Visibility = Visibility.Hidden;

            //String amend_server = server_list.SelectedItem.ToString();
            //string[] amend_split = amend_server.Split(' ');
            //name_input.Text = amend_split[0];
        }

        private void remove_server_Click(object sender, RoutedEventArgs e)
        {
            server_list_box.Visibility = Visibility.Visible;
            remove_server1.Visibility = Visibility.Visible;
            confirm_server.Visibility = Visibility.Hidden;

            for (int i = 0; i < manager_order.count_server(); i++)
            {
                server_list_box.Items.Add(manager_order.display_server(i));
            }
        }

        private void remove_server1_Click(object sender, RoutedEventArgs e)
        {
            //Removes the selected server from the list
            manager_order.remove_server(server_list_box.SelectedIndex);
            server_list_box.ItemsSource = null;
            server_list_box.ItemsSource = manager_order.server_list();


        }

        private void add_driver_Click(object sender, RoutedEventArgs e)
        {
            name_label.Visibility = Visibility.Visible;
            name_input.Visibility = Visibility.Visible;
            id_label.Visibility = Visibility.Visible;
            id_input.Visibility = Visibility.Visible;
            registration_label.Visibility = Visibility.Visible;
            registration_input.Visibility = Visibility.Visible;
            confirm_driver.Visibility = Visibility.Visible;
            driver_list_box.Visibility = Visibility.Hidden;
            remove_driver1.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Hidden;
        }

        private void confirm_driver_Click(object sender, RoutedEventArgs e)
        {
            //Add a new driver
            Driver new_driver = new Driver();
            //Set the relevant input to the right variables
            new_driver.First_name = name_input.Text;
            new_driver.Staff_id = Int32.Parse(id_input.Text);
            new_driver.Registration = registration_input.Text;
            
            driver_list.ItemsSource = null;
            manager_order.new_employee(new_driver);
            driver_list.ItemsSource = manager_order.driver_list();

            name_input.Clear();
            id_input.Clear();
        }

        private void amend_driver_Click(object sender, RoutedEventArgs e)
        {
            driver_list_box.Visibility = Visibility.Visible;
            remove_driver1.Visibility = Visibility.Hidden;
            confirm_driver.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Visible;
        }

        private void remove_driver_Click(object sender, RoutedEventArgs e)
        {
            remove_driver1.Visibility = Visibility.Visible;
            confirm_driver.Visibility = Visibility.Hidden;
            driver_list_box.Visibility = Visibility.Visible;
            name_label.Visibility = Visibility.Hidden;
            name_input.Visibility = Visibility.Hidden;
            id_label.Visibility = Visibility.Hidden;
            id_input.Visibility = Visibility.Hidden;
            registration_label.Visibility = Visibility.Hidden;
            registration_input.Visibility = Visibility.Hidden;
            amend_driver1.Visibility = Visibility.Hidden;
        }

        private void remove_driver1_Click(object sender, RoutedEventArgs e)
        {
            //Removes the selected server from the list
            manager_order.remove_driver(driver_list_box.SelectedIndex);
            driver_list_box.ItemsSource = null;
            driver_list_box.ItemsSource = manager_order.driver_list();
        }
    }
}