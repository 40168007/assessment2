﻿//Calum McCall
//This class contains the MainWindow class, its paramaters and verification.
//Last modified 10/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //New instance of order created
        Order order = new Order();
        //List of the menu items in this list
        List<Menu_item> bill = new List<Menu_item>();
        public MainWindow()
        {
            InitializeComponent();

            //5 menu items added
            Menu_item item_1 = new Menu_item();
            item_1.Description = "Steak \t £";
            item_1.Price = 13;
            item_1.Vegetarian = false;
            order.add_menu_item(item_1);

            Menu_item item_2 = new Menu_item();
            item_2.Description = "Salmon \t £";
            item_2.Price = 12;
            item_2.Vegetarian = false;
            order.add_menu_item(item_2);

            Menu_item item_3 = new Menu_item();
            item_3.Description = "Gnocchi \t £";
            item_3.Price = 9;
            item_3.Vegetarian = true;
            order.add_menu_item(item_3);

            Menu_item item_4 = new Menu_item();
            item_4.Description = "Soup \t £";
            item_4.Price = 7;
            item_4.Vegetarian = true;
            order.add_menu_item(item_4);

            Menu_item item_5 = new Menu_item();
            item_5.Description = "Fajitas \t £";
            item_5.Price = 8;
            item_5.Vegetarian = false;
            order.add_menu_item(item_5);

            //Lists the menu items
            Menu_item_list.ItemsSource = order.menu();

            //3 Servers added
            Server server_0 = new Server();
            server_0.First_name = "Rob";
            server_0.Staff_id = 0;
            order.new_employee(server_0);

            Server server_1 = new Server();
            server_1.First_name = "Ted";
            server_1.Staff_id = 1;
            order.new_employee(server_1);

            Server server_2 = new Server();
            server_2.First_name = "Dawn";
            server_2.Staff_id = 2;
            order.new_employee(server_2);

            Server server_3 = new Server();
            server_3.First_name = "Jamie";
            server_3.Staff_id = 3;
            order.new_employee(server_3);

            //Fills display box with servers
            for (int i = 0; i < order.count_server(); i++)
            {
                server_box.Items.Add(order.display_server(i));
            }

            server_box.SelectedIndex = 0;

            //3 Drivers added
            Driver driver_0 = new Driver();
            driver_0.First_name = "Chris";
            driver_0.Staff_id = 0;
            driver_0.Registration = "RE7 OW1";
            order.new_employee(driver_0);

            Driver driver_1 = new Driver();
            driver_1.First_name = "Tom";
            driver_1.Staff_id = 1;
            driver_1.Registration = "CT5 PL2";
            order.new_employee(driver_1);

            Driver driver_2 = new Driver();
            driver_2.First_name = "Mary";
            driver_2.Staff_id = 2;
            driver_2.Registration = "UI4 QE7";
            order.new_employee(driver_2);

            Driver driver_3 = new Driver();
            driver_3.First_name = "Taylor";
            driver_3.Staff_id = 3;
            driver_3.Registration = "LO8 YW9";
            order.new_employee(driver_3);

            //Fills display box with drivers
            for (int i = 0; i < order.count_driver(); i++)
            {
                driver_box.Items.Add(order.display_driver(i));
            }

            driver_box.SelectedIndex = 0;
        }

        //Add menu item to the order
        private void Add_to_order_Click(object sender, RoutedEventArgs e)
        {
            Order_list.ItemsSource = null;
            bill.Add((Menu_item)Menu_item_list.SelectedItem);
            Order_list.ItemsSource = bill;
        }

        //Displays the relevant UI for a sit in order
        private void Sit_in_choice_Checked(object sender, RoutedEventArgs e)
        {
            driver_box.Visibility = Visibility.Hidden;
            server_box.Visibility = Visibility.Visible;
            table_lbl.Visibility = Visibility.Visible;
            table_input.Visibility = Visibility.Visible;
            customer_name_lbl.Visibility = Visibility.Hidden;
            customer_name_input.Visibility = Visibility.Hidden;
            customer_address_lbl.Visibility = Visibility.Hidden;
            customer_address_input.Visibility = Visibility.Hidden;
            create_bill.IsEnabled = true;
        }

        //Displays the relevant UI for a delivery order
        private void Delivery_choice_Checked(object sender, RoutedEventArgs e)
        {
            driver_box.Visibility = Visibility.Visible;
            server_box.Visibility = Visibility.Hidden;
            table_lbl.Visibility = Visibility.Hidden;
            table_input.Visibility = Visibility.Hidden;
            customer_name_lbl.Visibility = Visibility.Visible;
            customer_name_input.Visibility = Visibility.Visible;
            customer_address_lbl.Visibility = Visibility.Visible;
            customer_address_input.Visibility = Visibility.Visible;
            create_bill.IsEnabled = true;
        }

        private void create_bill_Click(object sender, RoutedEventArgs e)
        {
            double price = 0;

            if (Sit_in_choice.IsChecked == true)
            {
                if (table_input.Text == "")
                {
                    MessageBox.Show("You haven't entered a table number");
                }
                else
                {
                    int table;
                    table = Int32.Parse(table_input.Text);
                    //Ensure table is appropriate number
                    if (table >= 1 && table <= 10)
                    {
                        //Adding the price of each dish to the total
                        foreach (Menu_item i in bill)
                        {
                            price += i.Price;
                        }

                        //Makes the order a sit in order
                        Sit_in s = new Sit_in();
                        //Adds the items to the bill
                        s.addItems((List<Menu_item>)bill);
                        order.new_sit_in(s);

                        //Moving the dishes to a string to display them
                        string bill_dishes = "";
                        foreach (Menu_item d in bill)
                        {
                            bill_dishes += d.Description + " " + d.Price + "\n";
                        }

                        MessageBox.Show("Thank you for eating at Neil & Ben’s Pizzeria " + "\n" + 
                                        "\n" +
                                        bill_dishes + "\n" +
                                        "Table number " + "\t" + table + "\n" +
                                        "Total price is " + "\t" + "£ " + price.ToString() + "\n" +
                                        "Served by " + "\t" + server_box.SelectedIndex.ToString());
                        //Clearing the form for another order
                        table_input.Clear();
                        //Order_list.ItemsSource = null;
                        //bill = null;
                    }
                }
            }
            else
            {
                //Adding the price of each dish to the total
                foreach (Menu_item i in bill)
                {
                    price += i.Price;
                }
                //Adding 15% for delivery
                price = price + (price * .15);
                Delivery d = new Delivery();
                d.addItems((List<Menu_item>)bill);
                order.new_delivery(d);
                //Moving the dishes to a string to display them
                string bill_dishes = "";
                foreach (Menu_item i in bill)
                {
                    bill_dishes += i.Description + " " + i.Price + "\n";
                }

                MessageBox.Show("Thank you for eating at Neil & Ben’s Pizzeria " + "\n" +
                                "\n" +
                                bill_dishes + "\n" +
                                "Name: " + "\t" + customer_name_input.Text + "\n" +
                                "Address: " + "\t" + customer_address_input.Text + "\n" +
                                "Total price is " + "\t" + "£ " + price.ToString() + "\n" +
                                "Served by " + "\t" + driver_box.SelectedIndex.ToString());
                //Clearing the form for another order
                customer_name_input.Clear();
                customer_address_input.Clear();
                //Order_list.ItemsSource = null;
                //bill = null;

            }
        }

        //Allows only numbers to be input into the table input field
        private void table_input_TextChanged(object sender, TextChangedEventArgs e)
        {
            char[] originalText = table_input.Text.ToCharArray();
            foreach (char c in originalText)
            {
                if (!(Char.IsNumber(c)))
                {
                    table_input.Text = table_input.Text.Remove(table_input.Text.IndexOf(c));
                }
            }
            table_input.Select(table_input.Text.Length, 0);
        }

        public void open_manager_Click(object sender, RoutedEventArgs e)
        {
            manager_panel manager = new manager_panel(order);
            manager.Show();
        }
    }
}
