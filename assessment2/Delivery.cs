﻿//Calum McCall
//This class contains the Delivery class, its paramaters and verification. This class inherits from the Order class
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    //This class inherits from the Order class
    class Delivery : Order
    {
        private string customer_name;    //The name of the customer for this delivery
        private string customer_address; //The address of the customer for this delivery
        private List<Menu_item> delivery_orders = new List<Menu_item>();    //List of all delivery orders

        public string Customer_name
        //Initialising and allowing access to customer_name
        {
            get
            {
                return customer_name;
            }
            set
            {
                //Makes sure a name has been entered
                if (value.Length == 0)
                {
                    throw new ArgumentException("Name has not been entered.");
                }
                //Sets the entered value to the name
                customer_name = value;
            }
        }

        public string Customer_address
        //Initialising and allowing access to customer_address
        {
            get
            {
                return customer_address;
            }
            set
            {
                //Makes sure an address has been 
                if (value.Length == 0)
                {
                    throw new ArgumentException("Address has not been entered.");
                }
                customer_address = value;
            }
        }

        public List<Menu_item> getItem()
        {
            return delivery_orders;
        }

        public void addItems(List<Menu_item> i)
        { 
            delivery_orders.AddRange(i);
        }
    }
}
