﻿//Calum McCall
//This class contains the Menu_item class, its paramaters and verification. //This class inherits from the Order class
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    //This class inherits from the Order class
    class Menu_item : Order
    {
        public Menu_item()
        {
        }

        private string description; //Description of the item
        private bool vegetarian;    //Is the item vegetarian?
        private double price;          //The price of the item

        public string Description
        //Initialising and allowing access to description
        {
            get
            {
                return description;
            }
            set
            {
                //Makes sure the field isn't empty as there must be a description
                if (value.Length == 0)
                {
                    throw new ArgumentException("No description entered.");
                }
                //Sets to value if requirement met
                description = value;
            }
        }

        public bool Vegetarian
        //Initialisng and allowing access to vegetarian
        {
            get
            {
                return vegetarian;
            }
            set
            {
                //Sets to the value
                vegetarian = value;
            }
        }

        public double Price
        //Initialising and allowing access to price
        {
            get
            {
                return price;
            }
            set
            {
                //Ensures price is at least 0 and not more than 100000(pence)
                if ((value < 0) || (value > 100000))
                {
                    throw new ArgumentException("No price entered.");
                }
                //Sets to value if between requirement
                price = value;
            }
        }

        //Converts the vegetarian boolean to a string
        public static string Veg(bool vegetarian)
        {
            if (vegetarian)
            {
                return "Yes";
            }

            return "No";
        }

        //Converts the menu item so it can be displayed
        public override string ToString()
        {
            return Description + price + "\t" + Veg(vegetarian);
        }
    }
}
