﻿//Calum McCall
//This class contains the Driver class, its paramaters and verification. This class inherits from the Employee class
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    //This class inherits from the Employee class
    class Driver : Employee
    {
        private string registration;    //The delivery drivers registration

        public string Registration
        //Initialsing and allows access to registration
        {
            get
            {
                return registration;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Registration not entered.");
                }
                registration = value;
            }
        }
    }
}
