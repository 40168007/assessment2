﻿//Calum McCall
//This class contains the Employee class, its paramaters and verification. This class inherits from the Order class
//Last modified 1/12/2015
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    //This class inherits from the Order class
    class Employee : Order
    {
        private string first_name;  //Employees first name
        private int staff_id;       //Employees staff_id

        public string First_name
        //Initialisng and allowing access to first_name
        {
            get
            {
                return first_name;
            }
            set
            {
                //Makes sure the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("First name not entered.");
                }
                //Sets to value if requirement met
                first_name = value;
            }
        }

        public int Staff_id
        //Initialising and allowing access to staff_id
        {
            get
            {
                return staff_id;
            }
            set
            {
                //Makes sure the field isn't empty
                if ((value < 0) || (value > 10))
                {
                    throw new ArgumentException("Staff_id must be between 0 and 10.");
                }
                //Sets to value if requirement met
                staff_id = value;
            }
        }
    }
}
